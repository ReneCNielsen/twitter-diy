# Twitter DIY

[![Binder](http://mybinder.org/badge.svg)](http://mybinder.org/repo/renecnielsen/twitter-diy)

This repository could potentially be the beginning of DIY toolkit to analyse tweets.

Thanks to [Jason Lustbader](https://github.com/jasonlustbader) and Tomaz Logar for getTweetsByID.py.

# Example Binder with requirements.txt

The `requirements.txt` file lists all Python libraries that the notebooks depend on. You can install them using

```
pip install -r requirements.txt
```

Note that many scientific Python libraries (e.g. `numpy`, `scipy`, `sklearn`, etc.) are included already because we use the [`base`](https://github.com/binder-project/binder/blob/master/images/base/Dockerfile) image for Binder (built on Anaconda).
